<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/deleteAccount/{id}', 'ProfileController@delete');

Route::get('/about', function () {
    return view('general-pages.about');
});

Route::get('/privacy-policy', function () {
    return view('general-pages.privacy-policy');
});

Route::get('/terms-and-conditions', function () {
    return view('general-pages.terms-and-conditions');
});

Route::get('/', 'HomeController@index')->name('home');

Route::post('/search', 'SearchController@index')->name('search');
Route::get('/search/more', 'SearchController@more')->name('searchMore');

Route::get('/profile/{username}', 'ProfileController@index')->name('profile');
Route::get('/updateLocation', 'ProfileController@updateLocationRender')->name('updateLocation');
Route::post('/updateLocation/go', 'ProfileController@updateLocationGo')->name('updateLocationGo');
Route::get('/uploadPhoto', 'ProfileController@uploadPhotoRender')->name('uploadPhoto');
Route::post('/uploadPhoto/go', 'ProfileController@uploadPhotoGo')->name('uploadPhotoGo');

Route::post('/question/submit', 'QuestionsController@store')->name('answerQuestion');
Route::get('/question/skip', 'QuestionsController@skip')->name('skipQuestion');

Route::get('/near-you', 'NearController@index')->name('near');
Route::get('/near-you/more', 'NearController@more')->name('nearMore');

Route::get('/answers/filter', 'AnswersController@filter')->name('filterAnswers');
Route::get('/answers/filter/more', 'AnswersController@filterMore')->name('filterAnswersMore');
Route::get('/answers/more', 'AnswersController@more')->name('moreAnswers');
Route::post('/answers/delete/{id}', 'AnswersController@delete')->name('deleteAnswer');

Route::get('/similar/loadMore', 'SimilarController@more');
Route::get('/similar/{id}', 'SimilarController@index');

Route::get('/follow/{username}', 'FeedController@update')->name('followStore');
Route::get('/unfollow/{username}', 'FeedController@destroy')->name('unfollowStore');
Route::get('/feed/viewProfile/{id}','FeedController@viewProfile');
Route::get('/feed/remove','FeedController@remove');
Route::get('/feed/alert','FeedController@updateAlert')->name('updateAlert');
Route::get('/feed', 'FeedController@index')->name('feed');

// ADMIN FUNCTIONS
Route::get('/admin/tools/sitemap', 'AdminController@sitemap');
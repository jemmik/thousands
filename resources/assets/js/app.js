require('./bootstrap');

window.fbAsyncInit = function() {
  FB.init({
    appId      : '257609321458966',
    xfbml      : true,
    version    : 'v3.0'
  });
  FB.AppEvents.logPageView();
  $(".facebook-share").on('click', function(){
      FB.ui({
        method: 'share_open_graph',
        action_type: 'share',
        action_properties: JSON.stringify({
          object: window.location.href,
        })
      }, function(response){
      });
  });
};
(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "https://connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));

if($("body#profile").length || $("body#similar").length || $("body#near").length || $("body#search").length) {
    document.addEventListener("DOMContentLoaded", function() {
        (adsbygoogle = window.adsbygoogle || []).push({});
    });
}

// Facebook Share
$(".share-facebook").on('click', function(){
    FB.ui({
    method: 'share',
    display: 'popup',
  }, function(response){});
});

// Registeration better date picker
if($("body#register").length){
    new Picker(document.querySelector('.js-date-picker'), {
      format: 'MM/DD/YYYY',
      text: {
          title: 'Pick A Date',
      }
    });

}

// Near you, load more results
$("body#near #more-results").on('click', function(e){
    e.preventDefault();
    $.ajax({
        url: "/near-you/more",
        success: function(results){
            $("body#near .results").html(results);
        }
    });
});

// Search, load more results
$("body#search #more-results").on('click', function(e){
    e.preventDefault();
    let place = parseInt($(this).attr('data-place'));
    let term = $(this).attr('data-term');
    let count = $(this).attr('data-count');
    $.ajax({
        url: "/search/more",
        data: { 'place': place, 'term': term },
        success: function(results){
            $("body#search .results").append(results);
            $("body#search #more-results").attr('data-place', place + 8);
            if(place + 8 >= count) {
                $("body#search .more-results").remove();
                $("body#search .content .container").append('<p class="end">End of search results.</p>');
            }
        }
    });
});

// Skip question
$(document).on('click',"body#profile .actions .skip", function(e){
    e.preventDefault();
    let blob = $("body#profile .blob").val();
    $.ajax({
        url: "/question/skip",
        data: { 'blob': blob },
        success: function(results){
            $("body#profile .questionsContainer .question").html(results);
            $("body#profile .actions .charCount span i").text(255);
        }
    });
});

// Questions answer fallback max characters
function maxLength(el) {
    if (!('maxLength' in el)) {
        var max = el.attributes.maxLength.value;
        el.onkeypress = function () {
            if (this.value.length >= max) return false;
        };
    }
}
if($("body#profile .questionsContainer .question").length){
    maxLength(document.getElementById("answer"));
}

// Count characters in questions answer
$(document).on('keyup',"body#profile textarea#answer", function(e){
    e.preventDefault();
    let maxLength = 255;
    let count = $(this).val().length;
    let remaining = maxLength - count;
    $("body#profile .actions .charCount span i").text(remaining);
})

// Check questions answer for length, submit if okay
$("body#profile .actions .submit").on('click', function(e){
    let count = $("body#profile textarea#answer").val().length;
    if(count < 1){
        alert('Your answer cannot be empty.');
    } else {
        $("body#profile #answerForm").submit();
    }
});

// FIlter category
$("body#profile .categoriesContainer select").on('change', function(){
    let category = $(this).find(":selected").val();
    if(category === ''){ location.reload(); }
    let username = $("body#profile #username").text();
    $.ajax({
        url: "/answers/filter",
        data: { 'category': category, 'username': username },
        success: function(results){
            $("body#profile .answersContainer").html(results);
            (adsbygoogle = window.adsbygoogle || []).push({});
        }
    });
});

// Load more answers
var used = [];
$(document).on('click',"body#profile .answersContainer .more-answers", function(e){
    e.preventDefault();
    let blob = $(this).attr('data-blob');
    let username = $("body#profile #username").text();
    $('body#profile .answersContainer .answer').each(function(key, value){
        used.push($(this).attr('data-id'));
    });
    $.ajax({
        url: "/answers/more",
        data: { 'username': username, 'blob': blob, 'used': used },
        success: function(results){
            $(document).find("body#profile .answersContainer .more-answers").attr('data-blob',blob).remove();
            $("body#profile .answersContainer").append(results);
            (adsbygoogle = window.adsbygoogle || []).push({});
        }
    });
});
$(document).on('click',"body#profile .answersContainer .more-answers-category", function(e){
    e.preventDefault();
    let blob = $(this).attr('data-blob');
    let username = $("body#profile #username").text();
    let category = $("body#profile .categoriesContainer select").find(':selected').val();
    $('body#profile .answersContainer .answer').each(function(key, value){
        used.push($(this).attr('data-id'));
    });
    $.ajax({
        url: "/answers/filter/more",
        data: { 'username': username, 'blob': blob, 'used': used, 'category': category },
        success: function(results){
            $(document).find("body#profile .answersContainer .more-answers-category").attr('data-blob',blob).remove();
            $("body#profile .answersContainer").append(results);
            (adsbygoogle = window.adsbygoogle || []).push({});
        }
    });
});

// Copy profile page url
$("body#profile .social .copyLink").on('click', function(e){
    e.preventDefault();
    let dummyContent = window.location.href;
    let dummy = $('<input>').val(dummyContent).appendTo('body').select();
    document.execCommand('copy');
    dummy.remove();
    alert('Your profile link has been copied to your clipboard');
});

// Back to Top
window.onscroll = function() {scrollFunction()};
function scrollFunction() {
    let button = $("#toTop");
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
        button.addClass('active').removeClass('hide');
    } else {
        button.removeClass('active').addClass('hide');
    }
}
$("#toTop").on('click', function(){
    $("html, body").animate({ scrollTop: 0 }, "slow");
});

// Similar load More
$(document).on('click', "body#similar .more-results", function(e){
    e.preventDefault();
    $(document).find("body#similar .more-results").remove();
    let id = $(this).attr('data-blob');
    $('body#similar .similar-container .answer').each(function(key, value){
        used.push($(this).attr('data-blob'));
    });
    $.ajax({
        url: "/similar/loadMore",
        data: { 'id': id, 'blob': used },
        success: function(results){
            $("body#similar .similarContainer").append(results);
        }
    });
});

// Feed page
if($("body#feed").length) {
    // Sidebar Search
    $("body#feed #followList").on('keyup', function(e){
        e.preventDefault();
        let search = $(this).val().toLowerCase();
        $("body#feed .followList .item").filter(function(){
            $(this).toggle($(this).text().toLowerCase().indexOf(search) > -1);
        });
    });
    $("body#feed .feedContainer .item  .remove").on('click', function(e){
        e.preventDefault();
        let blob = $(this).attr('data-blob');
        $.ajax({
            url: "/feed/remove",
            data: { 'blob': blob },
            success: function(results){
                $("body#feed .feedContainer .item .remove[data-blob='" + blob +"']").parent().fadeOut('fast');
            }
        });
    });
}

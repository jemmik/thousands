<footer id="footer" class="row">
    <div class="container flex-container">
        <nav id="social">
            <a href="https://www.facebook.com/Thousands-784409228614590/?modal=admin_todo_tour"
                target="_blank"
                rel="nofollow noreferrer"
                title="Thousands on Facebook"
                ><span><i class="fab fa-facebook-square"></i></span>
            </a>
            <a href="https://twitter.com/Thousands_me"
                target="_blank"
                rel="nofollow noreferrer"
                title="Thousands on Twitter"
                ><span><i class="fab fa-twitter-square"></i></span>
            </a>
        </nav>
        <div class="copyright">
            <p>Thousands.me {{ date('Y') }} <sup>©</sup>
                by<a href="https://jacobmorris.me"
                    target="_blank"
                    rel="noreferrer nofollow"
                    title="jacobmorris.me">
                    <span>Jacob Morris</span>
                </a>
            </p>
        </div>
        <nav id="links">
            <a href="{{ url('/privacy-policy') }}" title="Privacy Policy"><span>Privacy Policy</span></a>
            <a href="{{ url('/terms-and-conditions') }}" title="Terms and Conditions"><span>Terms & Conditions</span></a>
        </nav>
    </div>
</footer>

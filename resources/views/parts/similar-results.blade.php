@if(!empty($similar) && count($similar))
<div class="similar-container">
    @foreach($similar as $answer)
        <div class="answer similar" data-blob="{{ $answer->id }}">
            <p>{{ $answer->answer }}</p>
            <div class="divider"></div>
            <div class="info">
                <div class="poster">
                    <a href="{{ url('/profile/' . $answer->user->username) }}">
                        @if($answer->user->picture)
                            <img src="{!! asset('storage/uploads/profiles/' . $answer->user->picture ) !!}" alt="{{ $answer->user->name }}">
                        @else
                            <div class="placeholder">
                                <i class="fa fa-user"></i>
                            </div>
                        @endif
                        {{ $answer->user->name }}
                    </a>
                </div>
                <div class="toUser">
                    <a href="{{ url('/profile/' . $answer->user->username) }}">View Profile <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endif

@if($count >= 1)
<div class="more-results" data-blob="{{ $id }}">
    <span id="more-results">
        More Results
    </span>
</div>
@else
<p class="end">There are no more similar answers.</p>
@endif

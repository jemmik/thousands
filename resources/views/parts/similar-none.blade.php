@extends('layouts.app')
@section('title','Similar Answers')
@section('body-id','similar')
@section('body-class','fullHeight')

@section('content')
    <div class="row content">
        <div class="container home-content">
            <h1>Similar Answers</h1>
            <div class="answer">
                <h2>{{ $question }}</h2>
                <div class="divider"></div>
                <p>{{ $answer }}</p>
                <div class="divider"></div>
                <div class="info">
                    <div class="poster">
                        <a href="{{ url('/profile/' . $user->username) }}">
                            @if($user->picture)
                                <img src="{!! asset('storage/uploads/profiles/' . $user->picture ) !!}" alt="{{ $user->name }}">
                            @else
                                <div class="placeholder">
                                    <i class="fa fa-user"></i>
                                </div>
                            @endif
                            {{ $user->name }}
                        </a>
                    </div>
                    <div class="toUser">
                        <a href="{{ url('/profile/' . $user->username) }}">Back to profile <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <p class="end">There are no similar answers.</p>
            <div class="ad-container">
                <!-- Thousands feed ad -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-0840401025364314"
                     data-ad-slot="3698551564"
                     data-ad-format="auto"></ins>
            </div>
        </div>
    </div>
@endsection

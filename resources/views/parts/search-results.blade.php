@foreach($results as $result)
<a href="profile/{{ $result->username }}" class="result">
    @if($result->picture)
        <img src="{!! asset('storage/uploads/profiles/' . $result->picture ) !!}" alt="{{ $result->name }}">
    @else
        <div class="placeholder">
            <i class="fa fa-user"></i>
        </div>
    @endif
    <h3>{{ $result->name }}</h3>
    <h4>{{ $result->username }}</h4>
    <p>{{ $result->location }}</p>
</a>
@endforeach

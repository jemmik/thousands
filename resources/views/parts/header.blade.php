<header id="header" class="row">
    <div class="container flex-container">
        <div class="logo">
            <span>Thousands</span>
        </div>
        <div class="search">
            <form class="search" action="{{ route('search') }}" method="post" autocomplete="off">
                {{ csrf_field() }}
                <input type="search" name="search" value="" placeholder="Search">
                <button type="submit" name="search_submit"><i class="fa fa-search"></i></button>
            </form>
        </div>
        <nav id="nav">
            @if (Route::has('login'))
                @auth
                @php $username = Auth::user()->username; @endphp
                    <a href="{{ url('profile/' . $username) }}">My Profile</a>
                    <a href="{{ route('feed') }}">Feed</a>
                    <a href="{{ route('near') }}">Near You</a>
                    <a href="{{ route('logout') }}">Logout</a>
                @else
                    <a href="{{ route('login') }}">Login</a>
                    <a href="{{ route('register') }}">Register</a>
                    <a href="{{ url('/about') }}">About</a>
                @endauth
            @endif
        </nav>
    </div>
</header>
@if (session('success'))
    <div class="row alert" role="alert">
        <div class="container">
            <p>{{ session('success') }}</p>
        </div>
    </div>
@endif
@if (session('status'))
    <div class="row alert" role="alert">
        <div class="container">
            <p>{{ session('status') }}</p>
        </div>
    </div>
@endif

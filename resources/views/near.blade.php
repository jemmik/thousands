@extends('layouts.app')
@section('title','People near you')
@section('body-id','near')
@section('body-class','fullHeight')
@section('header-meta')
    <meta name="description" content="Find the people near you on Thousands.me" />
@endsection

@section('content')
    <div class="row content">
        <div class="container home-content">
            <h1>People Near You</h1>
            <div class="results @if(count($results) == 4 || count($results) == 8)full @endif">
                @include('parts.near-results')
            </div>
            @if(count($results))
            <div class="more-results">
                <span id="more-results">
                    See More People
                </span>
            </div>
            @else
            <p class="end">There is no users around you.</p>
            @endif
            <div class="ad-container">
                <!-- Thousands feed ad -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-0840401025364314"
                     data-ad-slot="3698551564"
                     data-ad-format="auto"></ins>
            </div>
        </div>
    </div>
@endsection

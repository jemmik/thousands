@extends('layouts.app')
@section('title','Search Results')
@section('body-id','search')
@section('body-class','fullHeight')
@section('header-meta')
    <meta name="description" content="Find people by their name or username." />
@endsection

@section('content')
    <div class="row content">
        <div class="container">
            <h1>Search Results For: {{ $term }}</h1>
            <div class="results">
                @if(count($results))
                    @include('parts.search-results')
                @else
                    <p>There was no results. Refine and search term and try again.</p>
                @endif
            </div>
            @if($count > 8)
                <div class="more-results">
                    <span id="more-results" data-count="{{ $count }}" data-place="8" data-term="{{ $term }}">
                        More Results
                    </span>
                </div>
            @else
                <p class="end">End of search results.</p>
            @endif
            <div class="ad-container">
                <!-- Thousands feed ad -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-0840401025364314"
                     data-ad-slot="3698551564"
                     data-ad-format="auto"></ins>
            </div>
        </div>
    </div>
@endsection

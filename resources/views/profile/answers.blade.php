@if(!empty($answers) && count($answers))
    @foreach($answers as $answer)
        <div class="answer" data-id="{{ $answer->id }}">
            @if(Auth::check() && Auth::user()->id == $user->id)
            <form action="{{ url('/answers/delete/' . $answer->id) }}" method="post">
                @csrf
                <input type="hidden" name="user_id" value="{{ $user->id }}">
                <button type="submit" name="delete"><i class="fa fa-trash"></i></button>
            </form>
            @endif
            <h3>{{ $answer->question }}</h3>
            <div class="divider"></div>
            <p>
                {{ $answer->answer }}
            </p>
            <div class="tools">
                <a href="{{ url('similar/' . $answer->id) }}"><i class="fa fa-clone"></i>Similar Answers</a>
                <!--<a href="#"><i class="fa fa-exclamation-circle"></i>Report Answer</a>-->
                <div class="answer-tools">
                    <!--<a href="#"><i class="fa fa-edit"></i>Edit Answer</a>-->
                </div>
            </div>
        </div>
    @endforeach
@endif
<div class="ad-container">
    <!-- Thousands feed ad -->
    <ins class="adsbygoogle"
         style="display:block"
         data-ad-client="ca-pub-0840401025364314"
         data-ad-slot="3698551564"
         data-ad-format="auto"></ins>
</div>

@if(count($answers))
    @if(empty($category))
        @if(empty($blob))
        <div class="more-answers" data-blob="1">
            <span>Load More Answers</span>
        </div>
        @else
            <div class="more-answers" data-blob="{{$blob}}">
                <span>Load More Answers</span>
            </div>
        @endif
        @else
        @if(empty($blob))
        <div class="more-answers-category" data-blob="1">
            <span>Load More Answers</span>
        </div>
        @else
            <div class="more-answers-category" data-blob="{{$blob}}">
                <span>Load More Answers</span>
            </div>
        @endif
    @endif
@else
    <p class="end">{{ $user->name }} has no more answered questions.</p>
@endif

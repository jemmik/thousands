<form method="POST" id="answerForm" action="{{ Route('answerQuestion') }}" aria-label="{{ __('Answer Question') }}"
     autocomplete="off">
    @csrf
    <input type="hidden" class="blob" name="blob" value="{{ $question->id }}">
    <div class="form-group">
        <label for="answer">{{ $question->question }}</label>
        <span class="label">{{ $question->question }}</span>
        <textarea name="answer" id="answer" rows="4" placeholder="Answer here..." maxlength="255"></textarea>
    </div>
</form>

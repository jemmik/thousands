<div class="social">
    <h4>Share On</h4>
    <span class="facebook-share" onclick="ga('send','event','click','Facebook')">Facebook</span>
    <a href="https://twitter.com/intent/tweet?text=Check out my answers on Thousands.me&hashtags=thousands&dnt=true&url={{ urlencode(url()->current()) }}"
         onclick="ga('send','event','click','Twitter')">Twitter</a>
    <span class="copyLink" onclick="ga('send','event','click','Copy')">Copy profile link</span>
</div>

<a href="{{ route('updateLocation') }}" class="action">
    <span>Update Location</span>
</a>

<a href="{{ route('uploadPhoto') }}" class="action">
    <span>Upload Photo</span>
</a>

<a href="{{ url('/deleteAccount/' . $user->id) }}" class="action">
    <span>Delete Account</span>
</a>

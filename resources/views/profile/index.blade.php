@extends('layouts.app')
@section('title'){{ $user->name }}@endsection
@section('body-id','profile')
@section('header-meta')
<meta property="og:url"           content="{{ url()->current() }}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Thousands | {{ $user->name }}" />
    <meta property="og:description"   content="Check out my answers on thousands.me" />
    @if($user->picture)
<meta property="og:image"         content="{!! asset('storage/uploads/profiles/' . $user->picture ) !!}" />
    @else
<meta property="og:image"         content="{{ asset('images/thousands.jpg') }}" />
    @endif
<meta property="fb:app_id"        content="257609321458966" />
<link rel="canonical" href="{{ url()->current() }}">
<meta name="description" content="The profile of {{ $user->name }}. See the answers to all their answered questions." />
@endsection
@section('body-scripts')
<div id="fb-root"></div>
<script src="//platform.twitter.com/widgets.js"></script>
@endsection

@section('content')
    <div class="row content">
        <div class="profile">
            <div class="container flex-container">
                <div class="profileContent">
                    <div class="icon">
                        @if($user->picture)
                            <img src="{!! asset('storage/uploads/profiles/' . $user->picture ) !!}" alt="{{ $user->name }}">
                        @else
                            <div class="placeholder">
                                <i class="fa fa-user"></i>
                            </div>
                        @endif
                    </div>
                    <div class="name">
                        I'm {{ $user->name }}
                    </div>
                    <div class="username">
                        My friends call me <div id="username" style="display: inline-block;">{{ $user->username }}</div>
                    </div>
                    <div class="age">
                        I am {{ $age }} years old
                    </div>
                    <div class="location">
                        I am from @if($location->city) {{$location->city}}@endif,@if($location->region) {{$location->region}} @endif<br />
                        @if($country) {{ $country }} @endif
                    </div>
                    <div class="count">
                        {{ $total }} answers
                    </div>
                    <div class="followers">
                        {{ $user->followers }} followers
                    </div>
                    <div class="profileActions">
                        @auth
                            @if(Auth::user()->id == $user->id)
                                @include('profile.profile-actions')
                            @else
                                @php
                                $follows = App\Feed::where('user_id', Auth::user()->id)->pluck('follows')->first();
                                $followed = 0;
                                if(!empty($follows)):
                                foreach($follows as $id => $count):
                                    if($id == $user->id):
                                        $followed = 1;
                                        break;
                                    endif;
                                endforeach;
                                endif;
                                @endphp
                                @if($followed == 0)
                                    <a href="{{ url('/follow/' . $user->username) }}" class="action"><span>Follow {{ $user->name }}</span></a>
                                @else
                                    <a href="{{ url('/unfollow/' . $user->username) }}" class="action"><span>Unfollow {{ $user->name }}</span></a>
                                @endif
                            @endif
                        @endauth
                    </div>
                    <button id="toTop" title="Go to top">Go To Top</button>
                </div>
            </div>
        </div>
        <div class="answers">
            <div class="container flex-container">
                @auth
                    @if(Auth::user()->id == $user->id)
                        @if(!empty($question))
                        <div class="questionsContainer">
                            <div class="question">
                                @include('profile.question')
                            </div>
                            <div class="actions">
                                <div class="charCount">
                                    <span>Characters: <i>255</i></span>
                                </div>
                                <div class="submit">
                                    <span>Submit Answer</span>
                                </div>
                                <div class="skip">
                                    <span>Skip Question</span>
                                </div>
                            </div>
                        </div>
                        @else
                            <p>You currently have no unaswered questions. Check back soon for more.</p>
                        @endif
                    @endif
                @endauth
                <div class="categoriesContainer">
                    <form method="POST" action="#" aria-label="{{ __('Filter By Category') }}"
                         autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <label for="name">{{ __('Filter By Category') }}</label>
                            <div class="category-block">
                                <select class="categoryPicker" name="categoryPicker">
                                    <option value="" selected>All Categories</option>
                                    <option value="1">Favorites</option>
                                    <option value="2">Politics & Religion</option>
                                    <option value="3">Lifestyle</option>
                                    <option value="4">This or That</option>
                                    <option value="5">Have You Ever</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="answersContainer">
                    @if(!empty($answers) && count($answers))
                        @include('profile.answers')
                    @else
                        <p>{{ $user->name }} has not answered any questions yet.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

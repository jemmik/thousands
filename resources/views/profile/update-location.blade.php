@extends('layouts.app')
@section('title','Update your location')
@section('body-id','profile')

@section('content')
    <div class="row content">
        <div class="profile">
            <div class="container flex-container">
                <div class="profileContent">
                    <div class="icon">
                        @if($user->picture)
                            <img src="{!! asset('storage/uploads/profiles/' . $user->picture ) !!}" alt="{{ $user->name }}">
                        @else
                            <div class="placeholder">
                                <i class="fa fa-user"></i>
                            </div>
                        @endif
                    </div>
                    <div class="name">
                        I'm {{ $user->name }}
                    </div>
                    <div class="username">
                        My friends call me {{ $user->username }}
                    </div>
                    <div class="age">
                        I am {{ $age }} years old
                    </div>
                    <div class="location">
                        I am from {{ $location->city }}, {{ $location->region}}<br />{{ $country }}
                    </div>
                    <div class="profileActions">
                        @auth
                            @if(Auth::user()->id == $user->id)
                                @include('profile.profile-actions')
                            @endif
                        @endauth
                    </div>
                </div>
            </div>
        </div>
        <div class="answers actionsContainer">
            <div class="container flex-container">
                <h1>Update your location</h1>
                <form method="POST" action="{{ route('updateLocationGo') }}" aria-label="{{ __('Update your location') }}"
                     autocomplete="off">
                    @csrf
                    <div class="form-group">
                        <label for="name">{{ __('Postal Code') }}</label>
                        <span class="label">What's your new Postal Code:</span>
                        <input id="location" type="location" class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}"
                            name="location" value="{{ old('location') }}" required>

                        @if ($errors->has('location'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('location') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                            <span>{{ __('Update Your Location') }}</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

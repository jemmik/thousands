@extends('layouts.app')
@section('title','Upload a photo')
@section('body-id','profile')

@section('content')
    <div class="row content">
        <div class="profile">
            <div class="container flex-container">
                <div class="profileContent">
                    <div class="icon">
                        @if($user->picture)
                            <img src="{!! asset('storage/uploads/profiles/' . $user->picture ) !!}" alt="{{ $user->name }}">
                        @else
                            <div class="placeholder">
                                <i class="fa fa-user"></i>
                            </div>
                        @endif
                    </div>
                    <div class="name">
                        I'm {{ $user->name }}
                    </div>
                    <div class="username">
                        My friends call me {{ $user->username }}
                    </div>
                    <div class="age">
                        I am {{ $age }} years old
                    </div>
                    <div class="location">
                        I am from {{ $location->city }}, {{ $location->region}}<br />{{ $country }}
                    </div>
                    <div class="profileActions">
                        @auth
                            @if(Auth::user()->id == $user->id)
                                @include('profile.profile-actions')
                            @endif
                        @endauth
                    </div>
                </div>
            </div>
        </div>
        <div class="answers actionsContainer">
            <div class="container flex-container">
                <h1>Upload a photo</h1>
                <form method="POST" action="{{ route('uploadPhotoGo') }}" aria-label="{{ __('Upload a photo') }}"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <span class="label">Pick your best photo: <small>(jpg or png)</small></span>
                        <label for="picture" class="pictureLabel">
                            <span><i class="fa fa-image"></i>{{ __('Select a photo') }}</span>
                            <input id="picture" type="file" accept="image/jpeg,image/x-png" class="form-control{{ $errors->has('picture') ? ' is-invalid' : '' }}"
                                name="picture" value="{{ old('picture') }}" required>
                        </label>

                        @if ($errors->has('location'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('location') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                            <span>{{ __('Upload Photo') }}</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

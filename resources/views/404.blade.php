@extends('layouts.app')
@section('title','404 Page not found!')
@section('body-id','error')
@section('body-class','fullHeight')

@section('content')
    <div class="row content">
        <div class="container">
            <h1>404 Page not found!</h1>
            <h2>It looks like you shouldn't be here, what happened?!?</h2>
            <p>You should probably get out of here before the internet police show up.. You do not want to deal with them!</p>
            <p>If you really feel like hanging out around here, go for it, I don't care. I'm not gonna tattle on you.</p>
            <p>But those internet police, I'm telling you, they do not play around..</p>
            <p>It's honestly in your best interest to just move on, find another page to goof around here. You'll
                thank yourself for it later.</p>
            <p>If this wasn't your fault, and something broke on our end, please <a href="mailto:contact@thousands.me">let us know</a>!</p>
        </div>
    </div>
@endsection

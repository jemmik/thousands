@extends('layouts.app')
@section('title','A place to get to know one another')
@section('body-id','home')
@section('body-class','fullHeight')
@section('header-meta')
<meta property="og:url"           content="{{ url()->current() }}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Thousands | A place to get to know one another" />
    <meta property="og:description"   content="Learn about your friends, family, colleagues, or even the mailman...
                                                by reading their answers.
                                                Answer questions about yourself so everyone can learn about you.
                                                Browse for people you don't know and maybe make some new friends." />
    <meta property="og:image"         content="{{ asset('images/thousands.jpg') }}" />
    <meta property="fb:app_id"        content="257609321458966" />
    <meta name="description" content="Learn about your friends, family, colleagues, or even the mailman...
                                                by reading their answers." />
@endsection

@section('content')
    <div class="row content">
        <div class="container home-content">
            <h1>Welcome To Thousands</h1>
            <form class="search" action="{{ route('search') }}" method="post" autocomplete="off">
                {{ csrf_field() }}
                <input type="search" name="search" value="" placeholder="Search">
                <button type="submit" name="search_submit"><i class="fa fa-search"></i></button>
            </form>
            <p>Thousands.me a place to get to know one another</p>
            <p>Learn about your friends, family, colleagues, or even the mailman...<br />
                by reading their answers.</p>
            <p>Answer questions about yourself so everyone can learn about you.</p>
            <p>Browse for people you don't know and maybe make some new friends.</p>
            <div class="gallery">
                <div class="image">
                    <img src="{{ asset('images/home/user-1.jpg') }}" alt="Thousands user">
                </div>
                <div class="image">
                    <img src="{{ asset('images/home/user-2.jpg') }}" alt="Thousands user">
                </div>
                <div class="image">
                    <img src="{{ asset('images/home/user-3.jpg') }}" alt="Thousands user">
                </div>
                <div class="image">
                    <img src="{{ asset('images/home/user-4.jpg') }}" alt="Thousands user">
                </div>
                <div class="image">
                    <img src="{{ asset('images/home/user-5.jpg') }}" alt="Thousands user">
                </div>
                <div class="image">
                    <img src="{{ asset('images/home/user-6.jpg') }}" alt="Thousands user">
                </div>
            </div>
        </div>
    </div>
@endsection

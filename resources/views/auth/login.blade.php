@extends('layouts.app')
@section('title','Login')
@section('body-id','login')
@section('body-class','fullHeight')

@section('content')
<div class="row content">
    <div class="container">
        <h1>Login To Thousands</h1>
        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}" autocomplete="off">
            @csrf
            <div class="form-group">
                <label for="name">{{ __('E-Mail') }}</label>
                <span class="label">What's your email:</span>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                    name="email" value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="name">{{ __('Password') }}</label>
                <span class="label">And your password:</span>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                    name="password" required>

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">
                    <span>{{ __('Login') }}</span>
                </button>
                <a class="forgot" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            </div>
        </form>
    </div>
</div>
@endsection

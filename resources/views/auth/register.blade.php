@extends("layouts.app")
@section("title","Register")
@section("body-id","register")
@section("header-scripts")
<link href="{{ asset('css/picker.min.css') }}" rel="stylesheet">
@endsection

@section("content")
<div class="row content">
    <div class="container">
        <h1>Register For An Account</h1>
        <form method="POST" action="{{ route("register") }}" aria-label="{{ __("Register") }}" autocomplete="off">
            @csrf
            <div class="form-group">
                <label for="name">{{ __("Name") }}</label>
                <span class="label">Tell us your name:</span>
                <input id="name" type="text" class="form-error{{ $errors->has("name") ? " is-invalid" : "" }}"
                    name="name" value="{{ old("name") }}" required autofocus>
                @if ($errors->has("name"))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first("name") }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="name">{{ __("Username") }}</label>
                <span class="label">Pick a username: <small>(This will be your page url)</small></span>
                <input id="username" type="text" class="form-error{{ $errors->has("username") ? " is-invalid" : "" }}"
                    name="username" value="{{ old("username") }}" required>
                @if ($errors->has("username"))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first("username") }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="name">{{ __("Email") }}</label>
                <span class="label">We need your email:</span>
                <input id="email" type="email" class="form-error{{ $errors->has("email") ? " is-invalid" : "" }}"
                    name="email" value="{{ old("email") }}" required>
                @if ($errors->has("email"))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first("email") }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="name">{{ __("Postal Code") }}</label>
                <span class="label">What is your postal code:</span>
                <input id="location" type="text" class="form-error{{ $errors->has("location") ? " is-invalid" : "" }}"
                    name="location" value="{{ old("location") }}" required>
                @if ($errors->has("location"))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first("location") }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="name">{{ __("Date of Birth") }}</label>
                <span class="label">When were you born:</span>
                <input id="dob" type="text" class="form-error{{ $errors->has("dob") ? " is-invalid" : "" }} js-date-picker"
                    name="dob" value="{{ old("dob") }}" required>
                @if ($errors->has("dob"))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first("dob") }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="name">{{ __("Password") }}</label>
                <span class="label">Pick a password:</span>
                <input id="password" type="password" class="form-error{{ $errors->has("password") ? " is-invalid" : "" }}"
                    name="password" value="{{ old("password") }}" required>
                @if ($errors->has("password"))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first("password") }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="name">{{ __("Confirm Password") }}</label>
                <span class="label">Confirm your password:</span>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">
                    <span>{{ __("Register") }}</span>
                </button>
            </div>
        </form>
    </div>
</div>
@endsection

@extends('layouts.app')
@section('title','Reset Password')
@section('body-id','login')
@section('body-class','fullHeight')

@section('content')
<div class="row content">
    <div class="container">
        <h1>Reset password</h1>
        <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}" autocomplete="off">
            @csrf
            <div class="form-group">
                <label for="name">{{ __('E-Mail') }}</label>
                <span class="label">What's your email:</span>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                    name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">
                    <span>{{ __('Send Password Reset Link') }}</span>
                </button>
            </div>
        </form>
    </div>
</div>
@endsection

@extends('layouts.app')
@section('title','Reset Password')
@section('body-id','login')
@section('body-class','fullHeight')

@section('content')
<div class="row content">
    <div class="container">
        <h1>Reset password</h1>
        <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}">
            @csrf

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group">
                <label for="name">{{ __('E-Mail') }}</label>
                <span class="label">What's your email:</span>
                <div class="col-md-6">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="name">{{ __('Password') }}</label>
                <span class="label">Choose your password:</span>
                <div class="col-md-6">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="name">{{ __('Confirm your password') }}</label>
                <span class="label">Confirm your password:</span>
                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">
                    <span>{{ __('Reset Password') }}</span>
                </button>
            </div>
        </form>
    </div>
</div>
@endsection

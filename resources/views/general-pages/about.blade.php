@extends('layouts.app')
@section('title','About Thousands.me')
@section('body-id','policy')
@section('body-class','fullHeight')
@section('header-meta')
    <meta name="description" content="Learn about your friends, family, and everyone else on Thousands.me.
                                        Answer questions and view the answers of others." />
@endsection

@section('content')
    <div class="row content">
        <div class="container">
            <h1>About Thousands.me</h1>
            <p>
                Users create accounts and answer questions about themselves so friends, family, neighbors, anyone can
                learn some about them. Users can also look up other users to view questions that others have answered
                and possibly make some new friends.
            </p>
        </div>
    </div>
@endsection

@foreach($list as $user)
    <div class="item">
        @if($user->picture)
            <img src="{!! asset('storage/uploads/profiles/' . $user->picture ) !!}" alt="{{ $user->name }}">
        @else
            <div class="placeholder">
                <i class="fa fa-user"></i>
            </div>
        @endif
        <p>
            <span class="name">
            {{ $user->name }}</span> answered
            @if($user->diff == 1)
            1 new question.
            @else
            {{ $user->diff }} new questions.
            @endif
        </p>
        <a href="{{ url('/feed/viewProfile/' . $user->id) }}" class="view">View Profile <i class="fa fa-arrow-right"></i></a>
        <div class="remove" data-blob="{{ $user->id }}"><i class="fa fa-trash"></i></div>
    </div>
@endforeach

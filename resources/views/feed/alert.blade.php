<div class="item">
    <p><i class="fa fa-bell"></i>{{ $alert->alert }}</p>
    @if($alert->action_text)
        <a href="{{ $alert->action_link }}" class="view">{{ $alert->action_text }} <i class="fa fa-arrow-right"></i></a>
    @endif
    <a href="{{ route('updateAlert') }}" class="remove"><i class="fa fa-trash"></i></a>
</div>

@extends('layouts.app')
@section('title','Your Feed')
@section('body-id','feed')
@section('body-class','fullHeight')

@section('content')
    <div class="row content">
        <div class="container">
            <h1>Your Feed</h1>
            <div class="content">
                <div class="main-content">
                    <h4>Hey {{ Auth::User()->name }}! Here is everything new today.</h4>
                    @if(!empty($alert))
                        <div class="alertContainer">
                            @include('feed.alert')
                        </div>
                    @endif
                    @if(!empty($list) && count($list))
                        <div class="feedContainer">
                            @include('feed.feed')
                        </div>
                    @else
                        <p class="end">You have nothing new on your feed.</p>
                    @endif
                </div>
                <aside id="sidebar">
                    <h2>People you follow</h2>
                    @if(empty($fullFollowing) && !count($fullFollowing))
                        <p class="end">You are not following anyone.</p>
                    @else
                        <input type="text" id="followList" placeholder="Search" />
                        <div class="followList">
                            @foreach($fullFollowing as $follow)
                                @foreach($follow as $user)
                                <div class="item" data-blob="{{ $user->id }}" class="in">
                                    <div class="user">
                                        @if($user->picture)
                                            <img src="{!! asset('storage/uploads/profiles/' . $user->picture ) !!}" alt="{{ $user->name }}">
                                        @else
                                            <div class="placeholder">
                                                <i class="fa fa-user"></i>
                                            </div>
                                        @endif
                                        <span class="name">{{ $user->name }}</span>
                                    </div>
                                    <a href="{{ url('/profile/' . $user->username ) }}">View</a>
                                </div>
                                @endforeach
                            @endforeach
                        </div>
                    @endif
                </aside>
            </div>
        </div>
    </div>
@endsection

<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use App\User;
use Auth;
use Redirect;
use Session;
use Illuminate\Http\Request;

class AnswersController extends Controller
{
    public function filter(Request $request)
    {
        $category = $request->category;
        $user = User::where('username', $request->username)->get()->first();

        $answersIds = $user->answers;
        if(!empty($answersIds)):
            $answers = Answer::join('questions','answers.question_id', '=', 'questions.id')->where('questions.category',$category)->whereIn('answers.id', $answersIds)->inRandomOrder()->limit(8)->get(['answers.id','answers.answer','questions.answer as question']);
        else:
            $answers = array();
        endif;

        return view('profile.answers',compact('answers','user','category'));
    }

    public function more(Request $request)
    {
        $user = User::where('username', $request->username)->get()->first();
        $answersIds = $user->answers;
        $used = $request->used;

        if(!empty($answersIds)):
            $answers = Answer::join('questions','answers.question_id', '=', 'questions.id')->whereIn('answers.id', $answersIds)->WhereNotIn('answers.id',$used)->inRandomOrder()->limit(8)->get(['answers.id','answers.answer','questions.answer as question']);
        else:
            $answers = array();
        endif;

        $blob = $request->blob;
        $blob++;

        return view('profile.answers',compact('answers','user','blob'));
    }

    public function filterMore(Request $request)
    {
        $category = $request->category;
        $user = User::where('username', $request->username)->get()->first();
        $answersIds = $user->answers;
        $used = $request->used;

        if(!empty($answersIds)):
            $answers = Answer::join('questions','answers.question_id', '=', 'questions.id')->where('questions.category',$category)->whereIn('answers.id', $answersIds)->WhereNotIn('answers.id',$used)->inRandomOrder()->limit(8)->get(['answers.id','answers.answer','questions.answer as question']);
        else:
            $answers = array();
        endif;

        $blob = $request->blob;
        $blob++;

        return view('profile.answers',compact('answers','user','blob','category'));
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $user = $request->user_id;
        if(Auth::check() && Auth::User()->id == $user):
            $user = Auth::user();
            $answer = Answer::where('id', $id)->delete();
            $answers = User::where('id', $user->id)->get()->pluck('answers')->first();
            $removeAnswers = array_search($id, $answers);
            unset($answers[$removeAnswers]);
            $user->answers = $answers;
            $answered = User::where('id', $user->id)->get()->pluck('answered')->first();
            $removedAnswered = array_search($id, $answered);
            unset($answered[$removedAnswered]);
            $user->answered = $answered;
            $user->save();
            Session::flash('success', 'Your answer has been deleted.');
            return redirect()->back();
        else:
            return redirect('/');
        endif;
    }
}

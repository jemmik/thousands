<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Answer;
use App\Question;

class SimilarController extends Controller
{
    public function index(Request $request)
    {
        $id = $request->id;
        $questionQuery = Answer::where('id', $id)->get(["question_id","answer","user_id"])->first();
        if(empty($questionQuery)):
            return view('404');
        endif;
        $user = User::where('id',$questionQuery->user_id)->get()->first();
        $question = Question::where('id', $questionQuery->question_id)->pluck('question')->first();
        $answer = $questionQuery->answer;
        $answerTerms = explode(' ', $answer);
        $reservedSymbols = ['-', '+', '<', '>', '@', '(', ')', '~'];
        $answerTerms = str_replace($reservedSymbols, '', $answer);
        $answerTerms = explode(' ', $answer);
        foreach($answerTerms as $key => $word) {
            if(strlen($word) >= 3) {
                $answerTerms[$key] = '+' . $word . '*';
            }
        }
        $answerTerms = implode( ' ', $answerTerms);

        $similar = Answer::where('question_id', $questionQuery->question_id)->where('user_id','<>', $questionQuery->user_id)
        ->selectRaw("MATCH (answer) AGAINST (? IN BOOLEAN MODE) AS score, answer as answer, user_id as user_id, id as id", [$answer])
        ->whereRaw("MATCH (answer) AGAINST (? IN BOOLEAN MODE)", $answer)->inRandomOrder()->limit(8)->get(["score","answer"]);
        if(!count($similar)):
            return view('parts.similar-none', compact('question','answer','user'));
        endif;
        $baseScore = $similar[0]->score;
        foreach($similar as $key => $result):
            if($result->score / $baseScore <  .5): unset($similar[$key]); endif;
        endforeach;
        foreach($similar as $key => $result):
            $result['user'] = User::where('id', $result->user_id)->get()->first();
        endforeach;

        $total = Answer::where('question_id', $questionQuery->question_id)->where('user_id','<>', $questionQuery->user_id)
        ->selectRaw("MATCH (answer) AGAINST (? IN BOOLEAN MODE) AS score, answer as answer, user_id as user_id, id as id", [$answer])
        ->whereRaw("MATCH (answer) AGAINST (? IN BOOLEAN MODE)", $answer)->get(["score","answer"]);
        $baseScore = $total[0]->score;
        foreach($total as $key => $result):
            if($result->score / $baseScore <  .5): unset($total[$key]); endif;
        endforeach;
        foreach($total as $key => $result):
            $result['user'] = User::where('id', $result->user_id)->get()->first();
        endforeach;

        $count = count($total) - 8;

        return view('similar', compact('question','answer','similar','user','count','id'));
    }

    public function more(Request $request)
    {
        $id = $request->id;
        $blob = $request->blob;

        $questionQuery = Answer::where('id', $id)->get(["question_id","answer","user_id"])->first();
        if(empty($questionQuery)):
            return view('404');
        endif;
        $user = User::where('id',$questionQuery->user_id)->get()->first();
        $question = Question::where('id', $questionQuery->question_id)->pluck('question')->first();
        $answer = $questionQuery->answer;
        $answerTerms = explode(' ', $answer);
        $reservedSymbols = ['-', '+', '<', '>', '@', '(', ')', '~'];
        $answerTerms = str_replace($reservedSymbols, '', $answer);
        $answerTerms = explode(' ', $answer);
        foreach($answerTerms as $key => $word) {
            if(strlen($word) >= 3) {
                $answerTerms[$key] = '+' . $word . '*';
            }
        }
        $answerTerms = implode( ' ', $answerTerms);

        $similar = Answer::where('question_id', $questionQuery->question_id)->whereNotIn('id', $blob)->where('user_id','<>', $questionQuery->user_id)
        ->selectRaw("MATCH (answer) AGAINST (? IN BOOLEAN MODE) AS score, answer as answer, user_id as user_id, id as id", [$answer])
        ->whereRaw("MATCH (answer) AGAINST (? IN BOOLEAN MODE)", $answer)->inRandomOrder()->limit(8)->get(["score","answer"]);
        $baseScore = $similar[0]->score;
        foreach($similar as $key => $result):
            if($result->score / $baseScore <  .5): unset($similar[$key]); endif;
        endforeach;
        foreach($similar as $key => $result):
            $result['user'] = User::where('id', $result->user_id)->get()->first();
        endforeach;

        $total = Answer::where('question_id', $questionQuery->question_id)->whereNotIn('id', $blob)->where('user_id','<>', $questionQuery->user_id)
        ->selectRaw("MATCH (answer) AGAINST (? IN BOOLEAN MODE) AS score, answer as answer, user_id as user_id, id as id", [$answer])
        ->whereRaw("MATCH (answer) AGAINST (? IN BOOLEAN MODE)", $answer)->get(["score","answer"]);
        $baseScore = $total[0]->score;
        foreach($total as $key => $result):
            if($result->score / $baseScore <  .5): unset($total[$key]); endif;
        endforeach;
        foreach($total as $key => $result):
            $result['user'] = User::where('id', $result->user_id)->get()->first();
        endforeach;

        $count = count($total) - 8;

        return view('parts.similar-results', compact('question','answer','similar','user','count','id'));
    }
}

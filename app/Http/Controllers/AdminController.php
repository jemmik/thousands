<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function sitemap()
    {
        if(empty($_GET['key']) || $_GET['key'] != 'thisisagoodkey'):
            return redirect('/');
        else:
            $content = '';
            $content .= '<?xml version="1.0" encoding="UTF-8"?>';
            $content .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

            $content .= '<url><loc>https://thousands.me</loc></url>';
            $content .= '<url><loc>https://thousands.me/register</loc></url>';
            $content .= '<url><loc>https://thousands.me/login</loc></url>';
            $content .= '<url><loc>https://thousands.me/privacy-policy</loc></url>';
            $content .= '<url><loc>https://thousands.me/terms-and-conditions</loc></url>';
            $content .= '<url><loc>https://thousands.me/about</loc></url>';
            $content .= '<url><loc>https://thousands.me/feed</loc></url>';
            $content .= '<url><loc>https://thousands.me/near-you</loc></url>';
            $content .= '<url><loc>https://thousands.me/register</loc></url>';

            $users = User::get()->pluck('username');
            foreach($users as $user):
                $content .= '<url><loc>https://thousands.me/profile/' . $user . '</loc></url>';
            endforeach;
                
            $content .= '</urlset>';

            return $content;
        endif;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Redirect;
use Session;
use DB;

use App\Feed;
use App\User;

class FeedController extends Controller
{
    public function index()
    {
        if(!Auth::user()):
            return redirect('/');
        endif;

        $feed = Feed::where('user_id', Auth::user()->id)->get()->first();
        $list = [];

        if(!empty($feed)):
            foreach($feed->follows as $id => $count):
                $user = User::where('id', $id)->get(['name','username','picture','answers','deleted_at','id'])->first();
                $answered = $user->answers;
                if(!empty($answered)):
                    $answered = count($answered);
                    if($answered > $count):
                        $diff = $answered - $count;
                        $user->diff = $diff;
                        $list[] = $user;
                    endif;
                endif;
            endforeach;
        else:
            $list = [];
        endif;

        $alert = DB::table('alerts')->orderBy('id','DESC')->get()->first();
        if(empty($alert)):
            $alert = "";
        endif;

        if(!empty($feed)):
            if($alert->id == $feed->alert_id):
                $alert = "";
            endif;
        endif;

        $fullFollowing = Feed::where('user_id', Auth::user()->id)->pluck('follows')->first();
        if(empty($fullFollowing)):
            $fullFollowing = [];
        else:
            foreach($fullFollowing as $id => $count):
                $user = User::where('id', $id)->get(['name','username','picture'])->first();
                $fullFollowing[$id] = array($user);
            endforeach;
        endif;

        return view('feed.index', compact('list','alert','fullFollowing'));
    }

    public function update(Request $request)
    {
        if(!Auth::user()):
            return redirect('/');
        endif;

        $username = $request->username;
        $followUser = User::where('username', $username)->get(['name','id','answers','followers'])->first();
        if(!empty($followUser->answers)):
            $followUserCount = count($followUser->answers);
        else:
            $followUserCount = 0;
        endif;

        $follows = Feed::where('user_id', Auth::user()->id)->get()->first();

        if(empty($follows)):
            $follow = new Feed;
            $follow->user_id = Auth::user()->id;
            $follow->follows = array($followUser->id => $followUserCount);
            $follow->alert_id = 0;
            $follow->save();
        else:
            $follow = Feed::where('user_id', Auth::user()->id)->get()->first();
            $followArray = $follow->follows;
            $followArray = $followArray + array($followUser->id => $followUserCount);
            $follow->follows = $followArray;
            $follow->save();
        endif;

        $followersCount = $followUser->followers;
        $followersCount++;
        $followUser->followers = $followersCount;
        $followUser->save();

        Session::flash('success', 'You have followed ' . $followUser->name . '.');
        return redirect()->back();
    }

    public function destroy(Request $request)
    {
        if(!Auth::user()):
            return redirect('/');
        endif;

        $username = $request->username;
        $unfollowUser = User::where('username', $username)->get(['name','id','answers','followers'])->first();

        $follows = Feed::where('user_id', Auth::user()->id)->get()->first();

        if(empty($follows) || empty($follows->follows)):
            return redirect('/');
        endif;

        $followsArray = $follows->follows;

        foreach($followsArray as $id => $count):
            if($unfollowUser->id == $id):
                unset($followsArray[$id]);
            endif;
        endforeach;

        $follows->follows = $followsArray;
        $follows->save();

        $followersCount = $unfollowUser->followers;
        $followersCount--;
        $unfollowUser->followers = $followersCount;
        $unfollowUser->save();

        Session::flash('success', 'You have unfollowed ' . $unfollowUser->name . '.');
        return redirect()->back();
    }

    public function updateAlert()
    {
        if(!Auth::check()):
            return redirect('/');
        else:
            $feed = Feed::where('user_id', Auth::user()->id)->get()->first();
            if(!empty($feed)):
                $currentAlert = DB::table('alerts')->orderBy('id', 'desc')->pluck('id')->first();
                $feed->alert_id = $currentAlert;
                $feed->save();
                return redirect()->back();
            else:
                $currentAlert = DB::table('alerts')->orderBy('id', 'desc')->pluck('id')->first();
                $feed = new Feed;
                $feed->user_id = Auth::user()->id;
                $feed->follows = [];
                $feed->alert_id = $currentAlert;
                $feed->save();
                return redirect()->back();
            endif;
        endif;
    }

    public function viewProfile(Request $request)
    {
        if(!Auth::check()):
            return redirect('/');
        endif;
        $id = $request->id;
        $user = User::where('id', $id)->get(['username','answers'])->first();
        $answersCount = count($user->answers);

        $feed = Feed::Where('user_id', Auth::user()->id)->get()->first();
        $follows = $feed->follows;
        $newFollows = $follows;

        foreach($newFollows as $followId => $count):
            if($id == $followId):
                $newFollows[$followId] = $answersCount;
            endif;
        endforeach;

        $feed->follows = $newFollows;
        $feed->save();

        return redirect('/profile/' . $user->username);
    }

    public function remove(Request $request)
    {
        if(!Auth::check()):
            return redirect('/');
        endif;

        $blob = $request->blob;
        $user = User::where('id', $blob)->get(['username','answers'])->first();
        $answersCount = count($user->answers);

        $feed = Feed::Where('user_id', Auth::user()->id)->get()->first();
        $follows = $feed->follows;
        $newFollows = $follows;

        foreach($newFollows as $followId => $count):
            if($blob == $followId):
                $newFollows[$followId] = $answersCount;
            endif;
        endforeach;

        $feed->follows = $newFollows;
        $feed->save();
    }
}

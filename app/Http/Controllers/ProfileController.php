<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Carbon;
use DB;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Storage;
use Image;
use File;

use App\Question;
use App\Answer;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($username)
    {
        // Get user from username
        $user = User::where('username', $username)->get()->first();
        if(empty($user)):
            return view('404');
        endif;

        // Calculate age
        $age = Carbon::createFromFormat("Y-m-d", $user->dob)->age;

        // Get location data
        $location = DB::table('locations')->where('postal', $user->location)->get()->last();

        $countries = array("AF" => "Afghanistan","AX" => "Aland Islands","AL" => "Albania","DZ" => "Algeria",
            "AS" => "American Samoa","AD" => "Andorra","AO" => "Angola","AI" => "Anguilla","AQ" => "Antarctica",
            "AG" => "Antigua And Barbuda","AR" => "Argentina","AM" => "Armenia","AW" => "Aruba","AU" => "Australia",
            "AT" => "Austria","AZ" => "Azerbaijan","BS" => "Bahamas","BH" => "Bahrain","BD" => "Bangladesh",
            "BB" => "Barbados","BY" => "Belarus","BE" => "Belgium","BZ" => "Belize","BJ" => "Benin","BM" => "Bermuda",
            "BT" => "Bhutan","BO" => "Bolivia","BA" => "Bosnia And Herzegovina","BW" => "Botswana","BV" => "Bouvet Island",
            "BR" => "Brazil","IO" => "British Indian Ocean Territory","BN" => "Brunei Darussalam","BG" => "Bulgaria",
            "BF" => "Burkina Faso","BI" => "Burundi","KH" => "Cambodia","CM" => "Cameroon","CA" => "Canada",
            "CV" => "Cape Verde","KY" => "Cayman Islands","CF" => "Central African Republic","TD" => "Chad",
            "CL" => "Chile","CN" => "China","CX" => "Christmas Island","CC" => "Cocos (Keeling) Islands",
            "CO" => "Colombia","KM" => "Comoros","CG" => "Congo","CD" => "Congo, Democratic Republic",
            "CK" => "Cook Islands","CR" => "Costa Rica","CI" => "Cote D'Ivoire","HR" => "Croatia","CU" => "Cuba",
            "CY" => "Cyprus","CZ" => "Czech Republic","DK" => "Denmark","DJ" => "Djibouti","DM" => "Dominica",
            "DO" => "Dominican Republic","EC" => "Ecuador","EG" => "Egypt","SV" => "El Salvador","GQ" => "Equatorial Guinea",
            "ER" => "Eritrea","EE" => "Estonia","ET" => "Ethiopia","FK" => "Falkland Islands (Malvinas)",
            "FO" => "Faroe Islands","FJ" => "Fiji","FI" => "Finland","FR" => "France","GF" => "French Guiana",
            "PF" => "French Polynesia","TF" => "French Southern Territories","GA" => "Gabon","GM" => "Gambia",
            "GE" => "Georgia","DE" => "Germany","GH" => "Ghana","GI" => "Gibraltar","GR" => "Greece",
            "GL" => "Greenland","GD" => "Grenada","GP" => "Guadeloupe","GU" => "Guam","GT" => "Guatemala",
            "GG" => "Guernsey","GN" => "Guinea","GW" => "Guinea-Bissau","GY" => "Guyana","HT" => "Haiti",
            "HM" => "Heard Island & Mcdonald Islands","VA" => "Holy See (Vatican City State)","HN" => "Honduras",
            "HK" => "Hong Kong","HU" => "Hungary","IS" => "Iceland","IN" => "India","ID" => "Indonesia",
            "IR" => "Iran, Islamic Republic Of","IQ" => "Iraq","IE" => "Ireland","IM" => "Isle Of Man","IL" => "Israel",
            "IT" => "Italy","JM" => "Jamaica","JP" => "Japan","JE" => "Jersey","JO" => "Jordan","KZ" => "Kazakhstan",
            "KE" => "Kenya","KI" => "Kiribati","KR" => "Korea","KW" => "Kuwait","KG" => "Kyrgyzstan",
            "LA" => "Lao People's Democratic Republic","LV" => "Latvia","LB" => "Lebanon","LS" => "Lesotho",
            "LR" => "Liberia","LY" => "Libyan Arab Jamahiriya","LI" => "Liechtenstein","LT" => "Lithuania",
            "LU" => "Luxembourg","MO" => "Macao","MK" => "Macedonia","MG" => "Madagascar","MW" => "Malawi",
            "MY" => "Malaysia","MV" => "Maldives","ML" => "Mali","MT" => "Malta","MH" => "Marshall Islands",
            "MQ" => "Martinique","MR" => "Mauritania","MU" => "Mauritius","YT" => "Mayotte","MX" => "Mexico",
            "FM" => "Micronesia, Federated States Of","MD" => "Moldova","MC" => "Monaco","MN" => "Mongolia",
            "ME" => "Montenegro","MS" => "Montserrat","MA" => "Morocco","MZ" => "Mozambique","MM" => "Myanmar",
            "NA" => "Namibia","NR" => "Nauru","NP" => "Nepal","NL" => "Netherlands","AN" => "Netherlands Antilles",
            "NC" => "New Caledonia","NZ" => "New Zealand","NI" => "Nicaragua","NE" => "Niger","NG" => "Nigeria",
            "NU" => "Niue","NF" => "Norfolk Island","MP" => "Northern Mariana Islands","NO" => "Norway",
            "OM" => "Oman","PK" => "Pakistan","PW" => "Palau","PS" => "Palestinian Territory, Occupied",
            "PA" => "Panama","PG" => "Papua New Guinea","PY" => "Paraguay","PE" => "Peru","PH" => "Philippines",
            "PN" => "Pitcairn","PL" => "Poland","PT" => "Portugal","PR" => "Puerto Rico","QA" => "Qatar",
            "RE" => "Reunion","RO" => "Romania","RU" => "Russian Federation","RW" => "Rwanda","BL" => "Saint Barthelemy",
            "SH" => "Saint Helena","KN" => "Saint Kitts And Nevis","LC" => "Saint Lucia","MF" => "Saint Martin",
            "PM" => "Saint Pierre And Miquelon","VC" => "Saint Vincent And Grenadines","WS" => "Samoa",
            "SM" => "San Marino","ST" => "Sao Tome And Principe","SA" => "Saudi Arabia","SN" => "Senegal",
            "RS" => "Serbia","SC" => "Seychelles","SL" => "Sierra Leone","SG" => "Singapore","SK" => "Slovakia",
            "SI" => "Slovenia","SB" => "Solomon Islands","SO" => "Somalia","ZA" => "South Africa",
            "GS" => "South Georgia And Sandwich Isl.","ES" => "Spain","LK" => "Sri Lanka","SD" => "Sudan",
            "SR" => "Suriname","SJ" => "Svalbard And Jan Mayen","SZ" => "Swaziland","SE" => "Sweden","CH" => "Switzerland",
            "SY" => "Syrian Arab Republic","TW" => "Taiwan","TJ" => "Tajikistan","TZ" => "Tanzania","TH" => "Thailand",
            "TL" => "Timor-Leste","TG" => "Togo","TK" => "Tokelau","TO" => "Tonga","TT" => "Trinidad And Tobago",
            "TN" => "Tunisia","TR" => "Turkey","TM" => "Turkmenistan","TC" => "Turks And Caicos Islands",
            "TV" => "Tuvalu","UG" => "Uganda","UA" => "Ukraine","AE" => "United Arab Emirates","GB" => "United Kingdom",
            "US" => "United States","UM" => "United States Outlying Islands","UY" => "Uruguay","UZ" => "Uzbekistan",
            "VU" => "Vanuatu","VE" => "Venezuela","VN" => "Viet Nam","VG" => "Virgin Islands, British",
            "VI" => "Virgin Islands, U.S.","WF" => "Wallis And Futuna","EH" => "Western Sahara","YE" => "Yemen",
            "ZM" => "Zambia","ZW" => "Zimbabwe",
            );

        foreach($countries as $key => $value):
            if($location->country == $key):
                $country = $value;
            endif;
        endforeach;

        $profileId = $user->id;
        if(Auth::check() && $user->id == Auth::user()->id):
            $answered = Auth::user()->answered;

            if(!empty($answered)):
                $question = Question::whereNotIn('id', $answered)->inRandomOrder()->get()->first();
            else:
                $question = Question::inRandomOrder()->get()->first();
            endif;
        else:
            $question = array();
        endif;

        $answersIds = $user->answers;
        if(!empty($answersIds)):
            $answers = Answer::join('questions','answers.question_id', '=', 'questions.id')->whereIn('answers.id', $answersIds)->inRandomOrder()->limit(8)->get(['answers.id','answers.answer','questions.answer as question']);
            $total = Answer::join('questions','answers.question_id', '=', 'questions.id')->whereIn('answers.id', $answersIds)->count();
        else:
            $answers = array();
            $total = 0;
        endif;

        // Return view with data
        return view('profile.index', compact('user','age','location','country','profileId','question','answers','total'));
    }

    public function updateLocationRender()
    {
        if(!Auth::check()):
            return Redirect::to('/');
        endif;

        // Get user from username
        $user = Auth::user();

        // Calculate age
        $age = Carbon::createFromFormat("Y-m-d", $user->dob)->age;

        // Get location data
        $location = DB::table('locations')->where('postal', $user->location)->get()->last();

        $countries = array("AF" => "Afghanistan","AX" => "Aland Islands","AL" => "Albania","DZ" => "Algeria",
            "AS" => "American Samoa","AD" => "Andorra","AO" => "Angola","AI" => "Anguilla","AQ" => "Antarctica",
            "AG" => "Antigua And Barbuda","AR" => "Argentina","AM" => "Armenia","AW" => "Aruba","AU" => "Australia",
            "AT" => "Austria","AZ" => "Azerbaijan","BS" => "Bahamas","BH" => "Bahrain","BD" => "Bangladesh",
            "BB" => "Barbados","BY" => "Belarus","BE" => "Belgium","BZ" => "Belize","BJ" => "Benin","BM" => "Bermuda",
            "BT" => "Bhutan","BO" => "Bolivia","BA" => "Bosnia And Herzegovina","BW" => "Botswana","BV" => "Bouvet Island",
            "BR" => "Brazil","IO" => "British Indian Ocean Territory","BN" => "Brunei Darussalam","BG" => "Bulgaria",
            "BF" => "Burkina Faso","BI" => "Burundi","KH" => "Cambodia","CM" => "Cameroon","CA" => "Canada",
            "CV" => "Cape Verde","KY" => "Cayman Islands","CF" => "Central African Republic","TD" => "Chad",
            "CL" => "Chile","CN" => "China","CX" => "Christmas Island","CC" => "Cocos (Keeling) Islands",
            "CO" => "Colombia","KM" => "Comoros","CG" => "Congo","CD" => "Congo, Democratic Republic",
            "CK" => "Cook Islands","CR" => "Costa Rica","CI" => "Cote D'Ivoire","HR" => "Croatia","CU" => "Cuba",
            "CY" => "Cyprus","CZ" => "Czech Republic","DK" => "Denmark","DJ" => "Djibouti","DM" => "Dominica",
            "DO" => "Dominican Republic","EC" => "Ecuador","EG" => "Egypt","SV" => "El Salvador","GQ" => "Equatorial Guinea",
            "ER" => "Eritrea","EE" => "Estonia","ET" => "Ethiopia","FK" => "Falkland Islands (Malvinas)",
            "FO" => "Faroe Islands","FJ" => "Fiji","FI" => "Finland","FR" => "France","GF" => "French Guiana",
            "PF" => "French Polynesia","TF" => "French Southern Territories","GA" => "Gabon","GM" => "Gambia",
            "GE" => "Georgia","DE" => "Germany","GH" => "Ghana","GI" => "Gibraltar","GR" => "Greece",
            "GL" => "Greenland","GD" => "Grenada","GP" => "Guadeloupe","GU" => "Guam","GT" => "Guatemala",
            "GG" => "Guernsey","GN" => "Guinea","GW" => "Guinea-Bissau","GY" => "Guyana","HT" => "Haiti",
            "HM" => "Heard Island & Mcdonald Islands","VA" => "Holy See (Vatican City State)","HN" => "Honduras",
            "HK" => "Hong Kong","HU" => "Hungary","IS" => "Iceland","IN" => "India","ID" => "Indonesia",
            "IR" => "Iran, Islamic Republic Of","IQ" => "Iraq","IE" => "Ireland","IM" => "Isle Of Man","IL" => "Israel",
            "IT" => "Italy","JM" => "Jamaica","JP" => "Japan","JE" => "Jersey","JO" => "Jordan","KZ" => "Kazakhstan",
            "KE" => "Kenya","KI" => "Kiribati","KR" => "Korea","KW" => "Kuwait","KG" => "Kyrgyzstan",
            "LA" => "Lao People's Democratic Republic","LV" => "Latvia","LB" => "Lebanon","LS" => "Lesotho",
            "LR" => "Liberia","LY" => "Libyan Arab Jamahiriya","LI" => "Liechtenstein","LT" => "Lithuania",
            "LU" => "Luxembourg","MO" => "Macao","MK" => "Macedonia","MG" => "Madagascar","MW" => "Malawi",
            "MY" => "Malaysia","MV" => "Maldives","ML" => "Mali","MT" => "Malta","MH" => "Marshall Islands",
            "MQ" => "Martinique","MR" => "Mauritania","MU" => "Mauritius","YT" => "Mayotte","MX" => "Mexico",
            "FM" => "Micronesia, Federated States Of","MD" => "Moldova","MC" => "Monaco","MN" => "Mongolia",
            "ME" => "Montenegro","MS" => "Montserrat","MA" => "Morocco","MZ" => "Mozambique","MM" => "Myanmar",
            "NA" => "Namibia","NR" => "Nauru","NP" => "Nepal","NL" => "Netherlands","AN" => "Netherlands Antilles",
            "NC" => "New Caledonia","NZ" => "New Zealand","NI" => "Nicaragua","NE" => "Niger","NG" => "Nigeria",
            "NU" => "Niue","NF" => "Norfolk Island","MP" => "Northern Mariana Islands","NO" => "Norway",
            "OM" => "Oman","PK" => "Pakistan","PW" => "Palau","PS" => "Palestinian Territory, Occupied",
            "PA" => "Panama","PG" => "Papua New Guinea","PY" => "Paraguay","PE" => "Peru","PH" => "Philippines",
            "PN" => "Pitcairn","PL" => "Poland","PT" => "Portugal","PR" => "Puerto Rico","QA" => "Qatar",
            "RE" => "Reunion","RO" => "Romania","RU" => "Russian Federation","RW" => "Rwanda","BL" => "Saint Barthelemy",
            "SH" => "Saint Helena","KN" => "Saint Kitts And Nevis","LC" => "Saint Lucia","MF" => "Saint Martin",
            "PM" => "Saint Pierre And Miquelon","VC" => "Saint Vincent And Grenadines","WS" => "Samoa",
            "SM" => "San Marino","ST" => "Sao Tome And Principe","SA" => "Saudi Arabia","SN" => "Senegal",
            "RS" => "Serbia","SC" => "Seychelles","SL" => "Sierra Leone","SG" => "Singapore","SK" => "Slovakia",
            "SI" => "Slovenia","SB" => "Solomon Islands","SO" => "Somalia","ZA" => "South Africa",
            "GS" => "South Georgia And Sandwich Isl.","ES" => "Spain","LK" => "Sri Lanka","SD" => "Sudan",
            "SR" => "Suriname","SJ" => "Svalbard And Jan Mayen","SZ" => "Swaziland","SE" => "Sweden","CH" => "Switzerland",
            "SY" => "Syrian Arab Republic","TW" => "Taiwan","TJ" => "Tajikistan","TZ" => "Tanzania","TH" => "Thailand",
            "TL" => "Timor-Leste","TG" => "Togo","TK" => "Tokelau","TO" => "Tonga","TT" => "Trinidad And Tobago",
            "TN" => "Tunisia","TR" => "Turkey","TM" => "Turkmenistan","TC" => "Turks And Caicos Islands",
            "TV" => "Tuvalu","UG" => "Uganda","UA" => "Ukraine","AE" => "United Arab Emirates","GB" => "United Kingdom",
            "US" => "United States","UM" => "United States Outlying Islands","UY" => "Uruguay","UZ" => "Uzbekistan",
            "VU" => "Vanuatu","VE" => "Venezuela","VN" => "Viet Nam","VG" => "Virgin Islands, British",
            "VI" => "Virgin Islands, U.S.","WF" => "Wallis And Futuna","EH" => "Western Sahara","YE" => "Yemen",
            "ZM" => "Zambia","ZW" => "Zimbabwe",
            );

        foreach($countries as $key => $value):
            if($location->country == $key):
                $country = $value;
            endif;
        endforeach;

        $profileId = $user->id;

        $question = Question::inRandomOrder()->get()->first();

        // Return view with data
        return view('profile.update-location', compact('user','age','location','country','profileId','question'));
    }

    public function updateLocationGo(Request $request)
    {
        if(!Auth::check() || empty($request)):
            return Redirect::to('/');
        else:
            $user = Auth::user();
            $location = $request->location;
            $match = DB::table('locations')->where('postal', $location)->get()->first();
            if(empty($match)):
                Session::flash('success', 'The Postal Code you entered is not valid.');
                return Redirect::back();
            endif;
            $user->location = $location;
            $user->save();
            Session::flash('success', 'Your location has been updated.');
            return Redirect::to('/profile/' . $user->username);
        endif;
    }

    public function uploadPhotoRender()
    {
        if(!Auth::check()):
            return Redirect::to('/');
        endif;

        // Get user from username
        $user = Auth::user();

        // Calculate age
        $age = Carbon::createFromFormat("Y-m-d", $user->dob)->age;

        // Get location data
        $location = DB::table('locations')->where('postal', $user->location)->get()->last();

        $countries = array("AF" => "Afghanistan","AX" => "Aland Islands","AL" => "Albania","DZ" => "Algeria",
            "AS" => "American Samoa","AD" => "Andorra","AO" => "Angola","AI" => "Anguilla","AQ" => "Antarctica",
            "AG" => "Antigua And Barbuda","AR" => "Argentina","AM" => "Armenia","AW" => "Aruba","AU" => "Australia",
            "AT" => "Austria","AZ" => "Azerbaijan","BS" => "Bahamas","BH" => "Bahrain","BD" => "Bangladesh",
            "BB" => "Barbados","BY" => "Belarus","BE" => "Belgium","BZ" => "Belize","BJ" => "Benin","BM" => "Bermuda",
            "BT" => "Bhutan","BO" => "Bolivia","BA" => "Bosnia And Herzegovina","BW" => "Botswana","BV" => "Bouvet Island",
            "BR" => "Brazil","IO" => "British Indian Ocean Territory","BN" => "Brunei Darussalam","BG" => "Bulgaria",
            "BF" => "Burkina Faso","BI" => "Burundi","KH" => "Cambodia","CM" => "Cameroon","CA" => "Canada",
            "CV" => "Cape Verde","KY" => "Cayman Islands","CF" => "Central African Republic","TD" => "Chad",
            "CL" => "Chile","CN" => "China","CX" => "Christmas Island","CC" => "Cocos (Keeling) Islands",
            "CO" => "Colombia","KM" => "Comoros","CG" => "Congo","CD" => "Congo, Democratic Republic",
            "CK" => "Cook Islands","CR" => "Costa Rica","CI" => "Cote D'Ivoire","HR" => "Croatia","CU" => "Cuba",
            "CY" => "Cyprus","CZ" => "Czech Republic","DK" => "Denmark","DJ" => "Djibouti","DM" => "Dominica",
            "DO" => "Dominican Republic","EC" => "Ecuador","EG" => "Egypt","SV" => "El Salvador","GQ" => "Equatorial Guinea",
            "ER" => "Eritrea","EE" => "Estonia","ET" => "Ethiopia","FK" => "Falkland Islands (Malvinas)",
            "FO" => "Faroe Islands","FJ" => "Fiji","FI" => "Finland","FR" => "France","GF" => "French Guiana",
            "PF" => "French Polynesia","TF" => "French Southern Territories","GA" => "Gabon","GM" => "Gambia",
            "GE" => "Georgia","DE" => "Germany","GH" => "Ghana","GI" => "Gibraltar","GR" => "Greece",
            "GL" => "Greenland","GD" => "Grenada","GP" => "Guadeloupe","GU" => "Guam","GT" => "Guatemala",
            "GG" => "Guernsey","GN" => "Guinea","GW" => "Guinea-Bissau","GY" => "Guyana","HT" => "Haiti",
            "HM" => "Heard Island & Mcdonald Islands","VA" => "Holy See (Vatican City State)","HN" => "Honduras",
            "HK" => "Hong Kong","HU" => "Hungary","IS" => "Iceland","IN" => "India","ID" => "Indonesia",
            "IR" => "Iran, Islamic Republic Of","IQ" => "Iraq","IE" => "Ireland","IM" => "Isle Of Man","IL" => "Israel",
            "IT" => "Italy","JM" => "Jamaica","JP" => "Japan","JE" => "Jersey","JO" => "Jordan","KZ" => "Kazakhstan",
            "KE" => "Kenya","KI" => "Kiribati","KR" => "Korea","KW" => "Kuwait","KG" => "Kyrgyzstan",
            "LA" => "Lao People's Democratic Republic","LV" => "Latvia","LB" => "Lebanon","LS" => "Lesotho",
            "LR" => "Liberia","LY" => "Libyan Arab Jamahiriya","LI" => "Liechtenstein","LT" => "Lithuania",
            "LU" => "Luxembourg","MO" => "Macao","MK" => "Macedonia","MG" => "Madagascar","MW" => "Malawi",
            "MY" => "Malaysia","MV" => "Maldives","ML" => "Mali","MT" => "Malta","MH" => "Marshall Islands",
            "MQ" => "Martinique","MR" => "Mauritania","MU" => "Mauritius","YT" => "Mayotte","MX" => "Mexico",
            "FM" => "Micronesia, Federated States Of","MD" => "Moldova","MC" => "Monaco","MN" => "Mongolia",
            "ME" => "Montenegro","MS" => "Montserrat","MA" => "Morocco","MZ" => "Mozambique","MM" => "Myanmar",
            "NA" => "Namibia","NR" => "Nauru","NP" => "Nepal","NL" => "Netherlands","AN" => "Netherlands Antilles",
            "NC" => "New Caledonia","NZ" => "New Zealand","NI" => "Nicaragua","NE" => "Niger","NG" => "Nigeria",
            "NU" => "Niue","NF" => "Norfolk Island","MP" => "Northern Mariana Islands","NO" => "Norway",
            "OM" => "Oman","PK" => "Pakistan","PW" => "Palau","PS" => "Palestinian Territory, Occupied",
            "PA" => "Panama","PG" => "Papua New Guinea","PY" => "Paraguay","PE" => "Peru","PH" => "Philippines",
            "PN" => "Pitcairn","PL" => "Poland","PT" => "Portugal","PR" => "Puerto Rico","QA" => "Qatar",
            "RE" => "Reunion","RO" => "Romania","RU" => "Russian Federation","RW" => "Rwanda","BL" => "Saint Barthelemy",
            "SH" => "Saint Helena","KN" => "Saint Kitts And Nevis","LC" => "Saint Lucia","MF" => "Saint Martin",
            "PM" => "Saint Pierre And Miquelon","VC" => "Saint Vincent And Grenadines","WS" => "Samoa",
            "SM" => "San Marino","ST" => "Sao Tome And Principe","SA" => "Saudi Arabia","SN" => "Senegal",
            "RS" => "Serbia","SC" => "Seychelles","SL" => "Sierra Leone","SG" => "Singapore","SK" => "Slovakia",
            "SI" => "Slovenia","SB" => "Solomon Islands","SO" => "Somalia","ZA" => "South Africa",
            "GS" => "South Georgia And Sandwich Isl.","ES" => "Spain","LK" => "Sri Lanka","SD" => "Sudan",
            "SR" => "Suriname","SJ" => "Svalbard And Jan Mayen","SZ" => "Swaziland","SE" => "Sweden","CH" => "Switzerland",
            "SY" => "Syrian Arab Republic","TW" => "Taiwan","TJ" => "Tajikistan","TZ" => "Tanzania","TH" => "Thailand",
            "TL" => "Timor-Leste","TG" => "Togo","TK" => "Tokelau","TO" => "Tonga","TT" => "Trinidad And Tobago",
            "TN" => "Tunisia","TR" => "Turkey","TM" => "Turkmenistan","TC" => "Turks And Caicos Islands",
            "TV" => "Tuvalu","UG" => "Uganda","UA" => "Ukraine","AE" => "United Arab Emirates","GB" => "United Kingdom",
            "US" => "United States","UM" => "United States Outlying Islands","UY" => "Uruguay","UZ" => "Uzbekistan",
            "VU" => "Vanuatu","VE" => "Venezuela","VN" => "Viet Nam","VG" => "Virgin Islands, British",
            "VI" => "Virgin Islands, U.S.","WF" => "Wallis And Futuna","EH" => "Western Sahara","YE" => "Yemen",
            "ZM" => "Zambia","ZW" => "Zimbabwe",
            );

        foreach($countries as $key => $value):
            if($location->country == $key):
                $country = $value;
            endif;
        endforeach;

        $profileId = $user->id;

        $question = Question::inRandomOrder()->get()->first();

        // Return view with data
        return view('profile.upload-photo', compact('user','age','location','country','profileId','question'));
    }

    public function uploadPhotoGo(Request $request)
    {
        if(!Auth::check() || empty($request)):
            return Redirect::to('/');
        else:
            $user = Auth::user();
            if($request->hasFile('picture')):
                $file = $request->file('picture');
                $extension = $file->getClientOriginalExtension();
                $size = getimagesize($file);
                $width = $size[0];
                $allowed = array('jpg','jpeg','png');
                if(!in_array($extension, $allowed)):
                    Session::flash('success', 'There was an error uploading your photo.');
                    return Redirect::to('/profile/' . $user->username);
                else:
                    $filename = $user->username . '.'.$extension;
                    $image = Image::make($file);
                    $image->fit(400, 400, function ($constraint) {
                        $constraint->upsize();
                    });
                    $resource = $image->stream()->detach();

                    Storage::disk('local')->put('public/uploads/profiles/' . $filename, $resource);
                    $user->picture = $filename;
                    $user->save();
                    Session::flash('success', 'Your photo has been uploaded.');
                    return Redirect::to('/profile/' . $user->username);
                endif;
            else:
                Session::flash('success', 'There was an error uploading your photo.');
                return Redirect::to('/profile/' . $user->username);
            endif;
        endif;
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        if(Auth::check() && Auth::user()->id == $id):
            User::find(Auth::user()->id)->delete();
            Session::flash('success', 'Your account has been deleted.');
            return redirect('/');
        else:
            return redirect('/');
        endif;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Redirect;
use DB;

class NearController extends Controller
{
    public function index()
    {
        if(!Auth::check()):
            return Redirect::to('/');
        endif;

        $user = Auth::user();
        $results = User::where([['location', $user->location],['id','!=',Auth::user()->id]])->inRandomOrder()->limit(8)->get();

        return view('near', compact('results'));
    }

    public function more()
    {
        $user = Auth::user();
        $results = User::where([['location', $user->location],['id','!=',Auth::user()->id]])->inRandomOrder()->limit(8)->get();

        return view('parts.near-results', compact('results'));
    }
}

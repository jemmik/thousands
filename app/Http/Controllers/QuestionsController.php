<?php

namespace App\Http\Controllers;

use App\Question;
use App\Answer;
use App\User;
use Auth;
use Redirect;
use Session;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    public function store(Request $request)
    {
        $answerCopy = $request->answer;
        $blob = $request->blob;

        $answer = new Answer;
        $answer->user_id = Auth::user()->id;
        $answer->question_id = $blob;
        $answer->answer = $answerCopy;
        $answer->save();
        $lastAnswerId = $answer->id;

        $user = User::find(Auth::user()->id);
        $answered = $user->answered;
        $answered[] = $blob;
        $user->answered = $answered;
        $answers = $user->answers;
        $answers[] = $lastAnswerId;
        $user->answers = $answers;
        $user->save();

        Session::flash('success', 'Your answer has been saved.');
        return Redirect()->back();
    }

    public function skip(Request $request)
    {
        $blob = $request->blob;
        $question = Question::where('id','!=',$blob)->inRandomOrder()->get()->first();
        return view('profile.question', compact('question'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Redirect;
use DB;

class SearchController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $term = $request->search;
        $results = User::where('name','like', '%' . $term . '%')->orWhere('username','like','%' . $term . '%')
                        ->orWhere('email',$term)->limit(8)->get();
        $count = User::where('name','like', '%' . $term . '%')->orWhere('username','like','%' . $term . '%')
                        ->orWhere('email',$term)->count();

        return view('search', compact('term','results','count'));
    }

    public function more(Request $request)
    {
        $place = $request->place;
        $term = $request->term;

        $results = User::where('name','like', '%' . $term . '%')->orWhere('username','like','%' . $term . '%')
                        ->orWhere('email',$term)->skip($place)->limit(8)->get();
        $count = User::where('name','like', '%' . $term . '%')->orWhere('username','like','%' . $term . '%')
                        ->orWhere('email',$term)->count();
        $count = $count - $place;
        return view('parts.search-more-results', compact('term','results','count'));
    }
}

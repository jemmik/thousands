<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    protected $fillable = [
        'follows',    
    ];
    protected $hidden = [
        'alert_id',
    ];
    protected $casts = [
        'follows' => 'array',
    ];
}
